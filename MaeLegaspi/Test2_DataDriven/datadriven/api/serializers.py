from rest_framework import serializers
from .models import Plan, PlanPrice, Cart, CartItem


class CannotUpdatePaidCart(serializers.ValidationError):

    def __init__(self):
        super().__init__({
            'cart': ['Cannot update a cart that is already paid.']
        }, 400)


class PlanListPricesSerializer(serializers.ModelSerializer):
    payment_term = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = PlanPrice
        fields = ('id', 'payment_term', 'price')


class PlanListSerializer(serializers.HyperlinkedModelSerializer):
    hmo = serializers.SlugRelatedField(slug_field='name', read_only=True)
    prices = serializers.SerializerMethodField(method_name='get_prices')

    class Meta:
        model = Plan
        fields = ('id', 'name', 'description', 'hmo', 'prices')

    def get_prices(self, instance):
        price_list = instance.prices.all().order_by('payment_term')
        return PlanListPricesSerializer(price_list, many=True).data


class CartItemPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = ('id', 'name')


class CartItemPlanPriceReadSerializer(serializers.ModelSerializer):
    plan = CartItemPlanSerializer(read_only=True)
    payment_term = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = PlanPrice
        fields = ('id', 'plan', 'payment_term', 'price')


class CartItemBaseSerializer(serializers.ModelSerializer):
    def validate_quantity(self, value):
        if value < 1:
            raise serializers.ValidationError('Quantity must be at least 1.', 400)
        return value

    def update(self, instance, validated_data):
        if instance.cart.paid:
            raise CannotUpdatePaidCart()
        return super().update(instance, validated_data)


class CartItemSerializer(CartItemBaseSerializer):
    plan_price = CartItemPlanPriceReadSerializer(read_only=True)

    class Meta:
        model = CartItem
        fields = ('id', 'plan_price', 'quantity')


class CartItemCreateSerializer(CartItemBaseSerializer):
    class Meta:
        model = CartItem
        fields = ('id', 'cart', 'plan_price', 'quantity')
        validators = []

    def create(self, validated_data):
        cart = validated_data['cart']
        if cart.paid:
            raise CannotUpdatePaidCart()

        new_quantity = validated_data.get('quantity', 1)

        cart_item, created = CartItem.objects.get_or_create(
            cart=cart,
            plan_price=validated_data['plan_price']
        )

        if created:
            cart_item.quantity = new_quantity
        else:
            cart_item.quantity += new_quantity

        cart_item.save()
        return cart_item


class CartListSerializer(serializers.HyperlinkedModelSerializer):
    items = CartItemSerializer(many=True, read_only=True)

    class Meta:
        model = Cart
        fields = ('id', 'paid', 'items')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and getattr(self.instance, 'paid', False):
            self.fields['paid'].read_only = True

    def update(self, instance, validated_data):
        if instance.paid:
            raise CannotUpdatePaidCart()
        return super().update(instance, validated_data)
