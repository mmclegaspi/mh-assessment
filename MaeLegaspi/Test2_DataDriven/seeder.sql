INSERT INTO payment_term (id, name) VALUES (1, 'Monthly');
INSERT INTO payment_term (id, name) VALUES (2, 'Quarterly');
INSERT INTO payment_term (id, name) VALUES (3, 'Annually');

INSERT INTO hmo (id, name) VALUES (1, 'Maxicare');
INSERT INTO hmo (id, name) VALUES (2, 'PhilCare');
INSERT INTO hmo (id, name) VALUES (3, 'MediCard');

INSERT INTO plan (id, hmo_id, name, description) VALUES (1, 1, 'Maxicare EReady', 'One-time emergency coverage up to ₱ 15,000 in Maxicare''s accredited hospitals');
INSERT INTO plan (id, hmo_id, name, description) VALUES (2, 1, 'MyMaxicare', 'All-inclusive health coverage and access to Maxicare''s network');
INSERT INTO plan (id, hmo_id, name, description) VALUES (3, 2, 'PhilCare ER Shield E-voucher', 'One-time emergency coverage up to PHP 50,000 for 6 months to 64 years old');
INSERT INTO plan (id, hmo_id, name, description) VALUES (4, 2, 'Philcare HealthPro', 'All-inclusive health coverage and access to Philcare''s network');

INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (1, 1, 3, 699.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (2, 2, 1, 2199.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (3, 2, 2, 6299.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (4, 2, 3, 24999.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (5, 3, 3, 800.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (6, 4, 1, 1700.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (7, 4, 2, 4600.00);
INSERT INTO plan_price (id, plan_id, payment_term_id, price) VALUES (8, 4, 3, 16700.00);
