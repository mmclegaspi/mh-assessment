CREATE TABLE "hmo" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(100) NOT NULL
);

CREATE TABLE "payment_term" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(25) NOT NULL
);

CREATE TABLE "plan" (
  "id" SERIAL PRIMARY KEY,
  "hmo_id" int NOT NULL,
  "name" varchar(100) NOT NULL,
  "description" varchar(255)
);

CREATE TABLE "plan_price" (
  "id" SERIAL PRIMARY KEY,
  "plan_id" int NOT NULL,
  "payment_term_id" int NOT NULL,
  "price" decimal(10, 2) NOT NULL
);

CREATE TABLE "cart" (
  "id" SERIAL PRIMARY KEY,
  "paid" boolean NOT NULL DEFAULT false
);

CREATE TABLE "cart_item" (
  "id" SERIAL PRIMARY KEY,
  "cart_id" int NOT NULL,
  "plan_price_id" int NOT NULL,
  "quantity" int NOT NULL DEFAULT 1
);

ALTER TABLE "plan" ADD FOREIGN KEY ("hmo_id") REFERENCES "hmo" ("id");
ALTER TABLE "plan_price" ADD FOREIGN KEY ("plan_id") REFERENCES "plan" ("id");
ALTER TABLE "plan_price" ADD FOREIGN KEY ("payment_term_id") REFERENCES "payment_term" ("id");
ALTER TABLE "cart_item" ADD FOREIGN KEY ("cart_id") REFERENCES "cart" ("id");
ALTER TABLE "cart_item" ADD FOREIGN KEY ("plan_price_id") REFERENCES "plan_price" ("id");

CREATE UNIQUE INDEX ON "cart_item" ("cart_id", "plan_price_id");