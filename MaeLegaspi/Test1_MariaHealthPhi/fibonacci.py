x = 50         # The number of fibonacci numbers to print

n2 = 0          # F(n-2)
n1 = 1          # F(n-1)

# Print first two numbers
if x >= 1:
    print(n2)
if x >= 2:
    print(n1)

# Print remaining numbers (or Maria/Health for multiples of 3/5)
for i in range(x-2):
    n = n2 + n1     # F(n)

    message = ""
    if (n % 3) == 0:
        message += "Maria "
    if (n % 5) == 0:
        message += "Health"
    if message:
        print(message.strip())
    else:
        print(n)

    n2 = n1
    n1 = n
