from django.urls import include, path
from rest_framework_nested import routers
from rest_framework_swagger.views import get_swagger_view
from . import views

router = routers.DefaultRouter()
router.register(r'plans', views.PlanViewSet)
router.register(r'carts', views.CartViewSet)

carts_router = routers.NestedDefaultRouter(router, r'carts', lookup='cart')
carts_router.register(r'items', views.CartItemViewSet, base_name='cart-items')

schema_view = get_swagger_view(title='DataDriven API')

urlpatterns = [
    path(r'swagger/', schema_view),
    path('', include(router.urls)),
    path('', include(carts_router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
