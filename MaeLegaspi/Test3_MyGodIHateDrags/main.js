let prefetchedData = [];
let currentSearchInfo = {};

$(document).ready(function () {
    hideNoMoreMessage();
    hideLoading();
    populateRouteOptions();
    populateTable();

    $("#clearButton").on("click", clearFilters);
    $("#searchForm").on("submit", populateTable);
});

$(window).on("scroll", function () {
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
        populateTableNextBatch();
    }
});

function resetCurrentSearchInfo() {
    currentSearchInfo = {
        limit: 10,
        offset: 0,
        nameFilter: $("#nameSearch").val().trim().replace(/\s/g, '+'),
        routeFilter: $("#routeSelect").val().trim().replace(/\s/g, '+'),
        estimatedTotal: 0
    };
}

function populateRouteOptions() {
    let routeOptions = [
        "oral", "topical", "intravenous", "dental", "respiratory", "ophthalmic",
        "intramuscular", "subcutaneous", "nasal", "rectal"
    ];
    for (let i = 0; i < routeOptions.length; i++) {
        let option = routeOptions[i];
        $("#routeSelect").append("<option value='" + option.toUpperCase() + "'>" + option.toUpperCase() + "</option>");
    }
}

function clearFilters() {
    $("#routeSelect").val("");
    $("#nameSearch").val("");

    populateTable();
}

function showLoading() {
    $("#tableLoading").show();
}

function hideLoading() {
    $("#tableLoading").hide();
}

function showNoMoreMessage() {
    $("#noMoreMessage").show();
}

function hideNoMoreMessage() {
    $("#noMoreMessage").hide();
}

function populateTable() {
    $("#drugsTable > tbody").empty();
    showLoading();
    hideNoMoreMessage();
    resetCurrentSearchInfo();
    prefetchedData = [];

    getDrugs(function (results) {
        if (results.length > 0) {
            addTableRows(results);
            prefetchDrugs();
        } else {
            showNoMoreMessage();
        }
        hideLoading();
    });

    return false;
}

function populateTableNextBatch() {
    if (prefetchedData.length > 0) {
        addTableRows(prefetchedData);
        prefetchedData = [];

        prefetchDrugs();
    } else {
        showNoMoreMessage();
    }
}

function prefetchDrugs() {
    if (currentSearchInfo.offset < currentSearchInfo.estimatedTotal) {
        getDrugs(function (results) {
            prefetchedData = results;
        });
    }
}

function getDrugs(callback) {
    let url = buildOpenFDAURL();

    $.getJSON(url, function (data, status) {
        console.log("Done retrieving data with filters: ", currentSearchInfo);
        if (status === "success") {
            currentSearchInfo.estimatedTotal = data.meta.results.total;
            currentSearchInfo.offset += data.results.length;
            callback(data.results);
        }
    }).fail(function () {
        currentSearchInfo.estimatedTotal = 0;
        callback([]);
    })
}

function buildOpenFDAURL() {
    let limit = currentSearchInfo.limit;
    let offset = currentSearchInfo.offset;
    let routeFilter = currentSearchInfo.routeFilter;
    let nameFilter = currentSearchInfo.nameFilter;

    let searchQuery = "_exists_:openfda";
    if (routeFilter) {
        searchQuery += '+AND+openfda.route:"' + routeFilter + '"';
    }
    if (nameFilter) {
        searchQuery += '+AND+(openfda.generic_name:"' + nameFilter + '"+OR+'
            + 'openfda.brand_name:"' + nameFilter + '")';
    }

    return "https://api.fda.gov/drug/label.json"
        + "?limit=" + limit + "&skip=" + offset
        + "&search=" + searchQuery;
}

function addTableRows(drugsData) {
    for (let i = 0; i < drugsData.length; i++) {
        let drugData = drugsData[i];

        let rowString = "<tr>";

        let openFDAData = drugData.openfda || {};
        let openFDAKeys = ["generic_name", "brand_name", "product_type", "route"];
        for (let j = 0; j < openFDAKeys.length; j++) {
            rowString += "<td>" + getDrugDataStr(openFDAData, openFDAKeys[j]) + "</td>";
        }

        let drugDataKeys = ["purpose", "indications_and_usage", "warnings", "active_ingredient",
            "inactive_ingredient", "storage_and_handling"];
        for (let j = 0; j < drugDataKeys.length; j++) {
            rowString += "<td>" + getDrugDataStr(drugData, drugDataKeys[j]) + "</td>";
        }

        rowString += "</tr>";

        $("#drugsTable").find("tbody:last").append(rowString);
    }
    console.log("Number of displayed rows: ", $("#drugsTable tbody tr").length);
}

function getDrugDataStr(data, key) {
    try {
        return data[key].join(", ");
    } catch (err) {
        return "";
    }
}
