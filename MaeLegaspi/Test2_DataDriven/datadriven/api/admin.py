from django.contrib import admin
from django.apps import apps
from . import models

# Register your models here.


class PlanPriceInline(admin.TabularInline):
    model = models.PlanPrice
    extra = 0


class CartItemInline(admin.TabularInline):
    model = models.CartItem
    extra = 0


class PlanAdmin(admin.ModelAdmin):
    inlines = [PlanPriceInline]


class CartAdmin(admin.ModelAdmin):
    inlines = [CartItemInline]


admin.site.register(models.Plan, PlanAdmin)
admin.site.register(models.Cart, CartAdmin)
