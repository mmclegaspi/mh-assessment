public class Fibonacci {

    public static void main(String[] args) {
        int x = 50;       // The number of fibonacci numbers to print

        long n2 = 0;      // F(n-2)
        long n1 = 1;      // F(n-1)

        // Print first two numbers
        if (x >= 1) {
            System.out.println(n2);
        }
        if (x >= 2) {
            System.out.println(n1);
        }

        // Print remaining numbers (or Maria/Health for multiples of 3/5)
        for (int i=0; i<x-2; i++) {
            long n = n2 + n1;     // F(n)

            String message = "";
            if ((n % 3) == 0) {
                message += "Maria ";
            }
            if ((n % 5) == 0) {
                message += "Health";
            }
            if (message != "") {
                System.out.println(message.trim());
            } else {
                System.out.println(n);
            }

            n2 = n1;
            n1 = n;
        }

    }

}