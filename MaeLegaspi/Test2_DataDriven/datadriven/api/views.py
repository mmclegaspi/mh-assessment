from rest_framework import viewsets
from rest_framework.response import Response

from . import serializers, models


# Create your views here.


class PlanViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Plan.objects.all().order_by('name')
    serializer_class = serializers.PlanListSerializer


class CartViewSet(viewsets.ModelViewSet):
    queryset = models.Cart.objects.all().order_by('id')
    serializer_class = serializers.CartListSerializer


class CartItemViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CartItemSerializer

    def get_queryset(self):
        return models.CartItem.objects.filter(cart=self.kwargs['cart_pk'])

    def create(self, request, *args, **kwargs):
        if 'cart' not in request.data and 'cart_pk' in self.kwargs:
            request.data['cart'] = self.kwargs['cart_pk']

        in_serializer = serializers.CartItemCreateSerializer(data=request.data)
        if in_serializer.is_valid():
            cart_item = in_serializer.save()
            out_serializer = serializers.CartItemSerializer(cart_item)
            return Response(out_serializer.data)
        else:
            return Response(in_serializer.errors, 400)

    def perform_destroy(self, instance):
        if instance.cart.paid:
            raise serializers.CannotUpdatePaidCart()
        return super().perform_destroy(instance)
