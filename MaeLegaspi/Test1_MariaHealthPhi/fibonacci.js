let x = 50;     // The number of fibonacci numbers to print

let n2 = 0;     // F(n-2)
let n1 = 1;     // F(n-1)

// Print first two numbers
if (x >= 1) {
    console.log(n2);
}
if (x >= 2) {
    console.log(n1);
}

// Print remaining numbers (or Maria/Health for multiples of 3/5)
for (let i=0; i<x-2; i++) {
    let n = n2 + n1;     // F(n)

    let message = "";
    if ((n % 3) === 0) {
        message += "Maria ";
    }
    if ((n % 5) === 0) {
        message += "Health";
    }
    if (message !== "") {
        console.log(message.trim());
    } else {
        console.log(n);
    }

    n2 = n1;
    n1 = n;
}
