from django.db import models

# Create your models here.


class HMO(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class PaymentTerm(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name


class Plan(models.Model):
    id = models.AutoField(primary_key=True)
    hmo = models.ForeignKey('HMO', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class PlanPrice(models.Model):
    id = models.AutoField(primary_key=True)
    plan = models.ForeignKey('Plan', on_delete=models.CASCADE, related_name='prices')
    payment_term = models.ForeignKey('PaymentTerm', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.plan.name} - {self.payment_term.name}"


class Cart(models.Model):
    id = models.AutoField(primary_key=True)
    paid = models.BooleanField(default=False)

    def __str__(self):
        return f"Cart {self.id} ({'Paid' if self.paid else 'Not Paid'})"


class CartItem(models.Model):
    id = models.AutoField(primary_key=True)
    cart = models.ForeignKey('Cart', on_delete=models.CASCADE, related_name='items')
    plan_price = models.ForeignKey('PlanPrice', on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)

    class Meta:
        unique_together = ('cart', 'plan_price')

    def __str__(self):
        return f"{self.plan_price} x {self.quantity}"
